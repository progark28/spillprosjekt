const app = require("express")();
const http = require("http").createServer(app);
const io = require("socket.io")(http);

app.get("/", (req, res) => {
  res.send("<h1>Hello ERIK</h1>");
});

let players = [];
let currentPlayer;

let startplayer = "";

function isReady() {
  if (players.length < 2) {
    return false;
  }
  for (let i = 0; i < players.length; i++) {
    if (!players[i].isReady) {
      return false;
    }
  }
  return true;
}

io.on("connection", (socket) => {
  console.log("a user connected");
  if (!currentPlayer) {
    currentPlayer = socket.id;
    console.log("startplayer ; " + currentPlayer);
  }

  socket.emit("socketID", { id: socket.id });

  if (players.length > 0) {
    socket.emit("updatePlayers", { id: players[0].id });
  }

  socket.on("playerReady", (playerID) => {
    console.log(playerID + " is ready");
    for (let i = 0; i < players.length; i++) {
      if (players[i].id == playerID) {
        players[i].isReady = true;
      }
    }
  });

  socket.on("isPlayersReady", () => {
    const ready = isReady();
    if (ready) {
      socket.emit("startMultiplayerGame", { ready, currentPlayer });
    }
  });

  socket.on("boxReleased", (x) => {
    socket.broadcast.emit("boxposition", { x });
  });

  socket.on("nextplayer", (playerID) => {
    socket.emit("nextplayer", { playerID });
    socket.broadcast.emit("nextplayer", { playerID });
  });

  socket.broadcast.emit("newPlayer", { id: socket.id });

  socket.on("disconnect", () => {
    console.log("user disconnected");
    socket.broadcast.emit("playerDisconnected", { id: socket.id });
    currentPlayer = "";
    startplayer = "";
    for (let i = 0; i < players.length; i++) {
      if (players[i].id == socket.id) {
        players.splice(i, 1);
      }
    }
  });
  players.push(new player(socket.id, false, 0));
});

http.listen(3000, () => {
  console.log("listening on *:3000");
});

function player(id, isReady, score) {
  this.id = id;
  this.isReady = isReady;
  this.score = score;
}
