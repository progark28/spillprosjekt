# Spillprosjekt

Klon prosjektet til egen mappe.
Åpne "build.gradle" i Android Studio for å koden klar til utvikling.

Spillprosjekt for TDT4240
Gruppe 28




===== SERVER ===== 


Hvordan sette opp server:

Installer NodeJS og NPM til din maskin (nyeste versjon burde være OK)

Gå inn i /Server mappa.   Hvis mappen: Node_Modules og/eller filen package_lock.json er i mappen må du slette de. 

Kjør "npm install", dette skal installere nodeJS-pakkene vi må ha.


Også må vi installere Socket.IO sin java-klient.  Dette gjør vi i filen "build.gradle".  

Nederst i filen, under "project:core / dependencies" må vi legge til en linje: _"implementation "io.socket:socket.io-gameClient:2.0.0"_

Det skal se omtrent slik ut: 

```
project(":core") {
    apply plugin: "java-library"


    dependencies {
        api "com.badlogicgames.gdx:gdx:$gdxVersion"
        api "com.badlogicgames.gdx:gdx-box2d:$gdxVersion"
        api "com.badlogicgames.ashley:ashley:$ashleyVersion"
        api "com.badlogicgames.box2dlights:box2dlights:$box2DLightsVersion"
        api "com.badlogicgames.gdx:gdx-freetype:$gdxVersion"
        implementation "io.socket:socket.io-gameClient:2.0.0"
    }
}
```



Når du endrer build.gradle får du mulighet å "synce" gradle, bare trykk ja til det.   

Da skal server være klar! Start serveren med kommandoen "npm start".


For å kjøre flere instanser av spillet må spillet kjøres fra terminal i stedet for "play"-knappen.  Kommandoen kjøres fra rot-mappa 
til prosjektet og er: 

"./gradlew desktop:run" 


