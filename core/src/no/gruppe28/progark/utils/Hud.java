package no.gruppe28.progark.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


public class Hud implements Disposable {
    public Stage stage;
    private Viewport viewport;

    private int[] scores;
    private int player, players;

    private Label curr_player, scoreLabel;

    public Hud(int players, Viewport viewportInput) {
        player = 0;

        this.players = players;
        scores = new int[players];

        for (int i = 0; i < scores.length; i++) {
            scores[i] = 0;
        }

        this.viewport = new StretchViewport(viewportInput.getWorldWidth(), viewportInput.getWorldHeight());
        this.stage = new Stage(viewport);

        Table table = new Table();
        table.top();
        table.setFillParent(true);

        curr_player = new Label(String.format("Player %01d", player + 1), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        if (scores.length == 2) {
            scoreLabel = new Label(String.format("%02d - %02d", scores[0], scores[1]), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        } else if (scores.length == 1) {
            scoreLabel = new Label(String.format("%02d", scores[0]), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        }

        table.add(curr_player).expandX().padTop(5);
        table.row();
        table.add(scoreLabel).expandX();
        stage.addActor(table);
    }

    public void setScore(int player) {
        scores[player] += 1;
    }

    public void changePlayer() {
        player = (player + 1) % players;

    }

    public int getPlayer() {
        return player;
    }

    public void update() {
        curr_player.setText(String.format("Player %01d", player + 1));
        if (scores.length == 2) {
            scoreLabel.setText(String.format("%02d - %02d", scores[0], scores[1]));
        } else if (scores.length == 1) {
            scoreLabel.setText(String.format("%02d", scores[0]));
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}
