package no.gruppe28.progark.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

import java.util.HashMap;

import no.gruppe28.progark.GdxSpill;
import no.gruppe28.progark.client.GameClient;
import no.gruppe28.progark.states.GameState;
import no.gruppe28.progark.states.LobbyState;
import no.gruppe28.progark.states.MenuState;
import no.gruppe28.progark.states.PauseState;
import no.gruppe28.progark.states.SettingsState;
import no.gruppe28.progark.states.WonState;

public class GameStateManager {
    public final GdxSpill app;

    private int players;

    private GameState gamestate;

    private HashMap<STATE, Screen> gameScreens;

    public enum STATE {
        MAIN_MENU,
        PLAY,
        PAUSE,
        SETTINGS,
        WON,
        LOBBY
    }

    private boolean gameFinished = false;


    public GameStateManager(final GdxSpill app) {
        this.app = app;


        initGameScreens();
        setScreen(STATE.MAIN_MENU);
    }

    private void initGameScreens() {
        this.gameScreens = new HashMap<>();
        this.gameScreens.put(STATE.MAIN_MENU, new MenuState(app));
      //  this.gameScreens.put(STATE.PLAY, new GameState(2, app));
        this.gameScreens.put(STATE.PAUSE, new PauseState(app));
        this.gameScreens.put(STATE.SETTINGS, new SettingsState(app));
        this.gameScreens.put(STATE.WON, new WonState(app));
        //this.gameScreens.put(STATE.LOBBY, new LobbyState(app, gameClient));
    }


    // == Tester INIt on-demand ==== == == ========= =
    public void initAndSetLobby() {
        app.gameClient = new GameClient(app);
        app.setScreen(new LobbyState(app));
    }

    public void initAndSetPlay(final int players) {
        Gdx.app.postRunnable(new Runnable() {
            public void run() {
                gamestate = new GameState(players, app);
                app.setScreen(gamestate);
            }
        });
    }

    // == == == === == ============ ==       ===========


    public void initPlay() {
        GameState gameState = new GameState(players, app);
        this.gameScreens.put(STATE.PLAY, gameState);
    }

    //TODO: sjekk hvis man legger inn en ekstra input her for int og antall spillere og dropper å initiate play i initgamescreens
    public void setScreen(STATE nextScreen, int players) {
        System.out.println("Er inne i setScreen");
        if (gameFinished || players != this.players) {
            this.players = players;
            initPlay();
            setGameFinished(false);
        }
        app.setScreen(gameScreens.get(nextScreen));
    }

    public void setScreen(STATE nextScreen) {
        if (gameFinished && nextScreen == STATE.PLAY) {
            initPlay();
            setGameFinished(false);
        }
        app.setScreen(gameScreens.get(nextScreen));
    }

    // TEST startGame() med on-deman init
    public void startGame() {
        initAndSetPlay(2);
    }

    public HashMap<STATE, Screen> getGameScreens() {
        return gameScreens;
    }

    public void dispose() {
        for (Screen state : gameScreens.values()) {
            if (state != null) {
                state.dispose();
            }
        }
    }

    public GameState getGamestate() {
        return gamestate;
    }

    public void setGameFinished(boolean gameFinished) {
        this.gameFinished = gameFinished;
    }

}
