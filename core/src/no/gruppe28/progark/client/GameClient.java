package no.gruppe28.progark.client;

import com.badlogic.gdx.Gdx;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import no.gruppe28.progark.GdxSpill;
import no.gruppe28.progark.manager.GameStateManager;
import no.gruppe28.progark.states.GameState;

public class GameClient {

    private Socket socket;
    private String id_player1 = "";
    private String id_player2 = "";

    private String currentPlayer = "";

    private String startPlayer = "";

    private boolean ready;

    private GdxSpill game;


    public GameClient(GdxSpill game) {
        connectSocket();
        configSocketEvents();

        this.game = game;
    }

    // == Connect to server ==

    private void connectSocket() {
        try {
            socket = IO.socket("http://localhost:3000/");
            socket.connect();
        } catch (Exception e) {
            System.out.println("Connection failed : " + e.getMessage());
        }
    }

    // == Update server methods ==

    public void playerIsReady() {

        socket.emit("playerReady", id_player1);

    }

    public void isPlayerReady() {
        if (!ready) {
            socket.emit("isPlayersReady");
        }
    }

    public void emitBox_posX(float x){
        socket.emit("boxReleased", x);
    }

    public void changePlayer(String playerID){
        socket.emit("nextplayer", playerID);
    }


    // == Handle messages from server method ==

    public void configSocketEvents() {
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Gdx.app.log("SocketIO", "Connected");
            }
        }).on("socketID", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    id_player1 = data.getString("id");
                    Gdx.app.log("SocketIO", "My ID: " + id_player1);
                } catch (JSONException e) {
                    Gdx.app.log("SocketIO", "Error getting ID");
                }
            }
        }).on("newPlayer", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    id_player2 = data.getString("id");
                    Gdx.app.log("SocketIO", "New Player Connected: " + id_player2);
                } catch (JSONException e) {
                    Gdx.app.log("SocketIO", "Error getting New PlayerID");
                }
            }
        }).on("updatePlayers", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    id_player2 = id_player2.equals("") ? data.getString("id") : id_player2;
                    Gdx.app.log("updatePlayers", "player2 ID: " + id_player2);
                } catch (JSONException e) {
                    Gdx.app.log("SocketIO", "Error getting ID");
                }
            }
        }).on("startMultiplayerGame", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    ready = data.getBoolean("ready");
                    currentPlayer = data.getString("currentPlayer");
                    if(startPlayer.length() == 0){
                        startPlayer = currentPlayer;
                    }
                    if (ready) {
                        startGame();
                    }
                } catch (JSONException e) {
                    Gdx.app.log("SocketIO", "Error getting value StartMultiplayerGame");
                }
            }
        }).on("boxposition", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    float positionX = (float) data.getDouble("x");
                    GameState currentGameState = game.gsm.getGamestate();
                    currentGameState.handleServerInput(positionX);

                } catch (JSONException e) {
                    Gdx.app.log("SocketIO", "Error getting value StartMultiplayerGame");
                }
            }
        }).on("nextplayer", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    String nextplayer = data.getString("playerID");
                    currentPlayer = nextplayer;
                } catch (JSONException e) {
                    Gdx.app.log("SocketIO", "Error getting value StartMultiplayerGame");
                }
            }
        }).on("playerDisconnected", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                id_player2 = "";

            }
        });

    }

    private void startGame() {
        game.gsm.startGame();
    }

    public String getCurrentPlayer() {
        return currentPlayer;
    }

    public java.lang.String getStartPlayer() {
        return startPlayer;
    }

    public String getId_player1() {
        return id_player1;
    }

    public String getId_player2() {
        return id_player2;
    }
}
