package no.gruppe28.progark;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import no.gruppe28.progark.client.GameClient;
import no.gruppe28.progark.manager.GameStateManager;
import no.gruppe28.progark.states.GameState;
import no.gruppe28.progark.states.MenuState;

public class GdxSpill extends Game {
	public SpriteBatch batch;
	public GameStateManager gsm;
	public GameClient gameClient;

	private Music music;
	private boolean sound = true;

	@Override
	public void create () {
		batch = new SpriteBatch();
		gsm = new GameStateManager(this);
		music = Gdx.audio.newMusic(Gdx.files.internal("sound/skate.mp3"));
		music.setLooping(true);
		music.setVolume(0.1f);

	}

	@Override
	public void render () {
		super.render();
		if(sound == true) {
			music.play();
		}
		if(sound == false) {
			music.pause();
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
			Gdx.app.exit();
		}
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		music.dispose();
	}

	public boolean isSound() {
		return sound;
	}

	public void setSound(boolean sound) {
		this.sound = sound;
	}
}
