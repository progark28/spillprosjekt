package no.gruppe28.progark.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import no.gruppe28.progark.GdxSpill;
import no.gruppe28.progark.manager.GameStateManager;
import no.gruppe28.progark.utils.Hud;

public class WonState implements Screen {
    private Label winnerPlayer;
    private Texture title;
    private TextureRegion background;
    private Image playBtn, menuBtn;

    private OrthographicCamera gamecam;
    private Viewport gamePort;

    private GdxSpill game;
    private Stage stage;

    private Hud hud;

    public WonState(final GdxSpill game){
        this.game = game;

        gamecam = new OrthographicCamera();
        gamePort = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), gamecam);
        stage = new Stage(gamePort, game.batch);
        hud = new Hud(1, gamePort);

        background = new TextureRegion(new Texture("backgroundblur.jpg"), 0, 0 , 4088, 2044);

        Table table = new Table();
        table.top();
        table.setFillParent(true);

        //TODO: evt endre font og size her hehe
        winnerPlayer = new Label(String.format("Player %01d is the winner", hud.getPlayer()+1), new Label.LabelStyle(new BitmapFont(), Color.BLACK));

        table.add(winnerPlayer).expandX().padTop(150);

        stage.addActor(table);

    }

    @Override
    public void show() {
        menuBtn = new Image(new Texture(Gdx.files.internal("menu.png")));
        menuBtn.setSize(menuBtn.getWidth()/2, menuBtn.getHeight()/2);
        menuBtn.setPosition(Gdx.graphics.getWidth()/2 - menuBtn.getWidth()/2, Gdx.graphics.getHeight() - menuBtn.getHeight()*4 + 10);
        menuBtn.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.gsm.setScreen(GameStateManager.STATE.MAIN_MENU);
                dispose();
            }
        });

        // TODO: må passe på at denne skiller mellom multiplayer og singleplayer. dvs hva skal være default på restart? Forslag er 1 i tilfelle ikke begge vil spille på nytt
        playBtn = new Image(new Texture(Gdx.files.internal("play.png"))); //Set the button up
        playBtn.setSize(playBtn.getWidth()/2, playBtn.getHeight()/2);
        playBtn.setPosition(Gdx.graphics.getWidth()/2 - playBtn.getWidth()/2, Gdx.graphics.getHeight() - playBtn.getHeight()*5);
        playBtn.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.gsm.setScreen(GameStateManager.STATE.PLAY, 1);
                dispose();
            }
        });

        title = new Texture("GameOver.png");

        stage.addActor(playBtn); //Add the button to the stage to perform rendering and take input.
        stage.addActor(menuBtn); //Add the button to the stage to perform rendering and take input.
        Gdx.input.setInputProcessor(stage); //Start taking input from the ui
    }


    @Override
    public void render(float dt) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.batch.begin();
        game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.draw(title, Gdx.graphics.getWidth()/2 - title.getWidth()/2, Gdx.graphics.getHeight() - title.getHeight());
        game.batch.end();

        stage.act(Gdx.graphics.getDeltaTime()); //Perform ui logic
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
