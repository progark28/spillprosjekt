package no.gruppe28.progark.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import no.gruppe28.progark.GdxSpill;
import no.gruppe28.progark.client.GameClient;
import no.gruppe28.progark.manager.GameStateManager;

public class LobbyState implements Screen {
    private Texture title, bgTexture;
    private TextureRegion background;
    private Image backBtn;
    private Image playBtn;

    private boolean playerIsReady;

    private OrthographicCamera gamecam;
    private Viewport gamePort;

    private BitmapFont font;

    private GdxSpill game;
    private Stage stage;

    private GameClient gameClient;


    private Texture player1;
    private Texture player2;

    //Hud hud;

    private boolean isTwoPlayers() {
        return gameClient.getId_player1().length() > 0 && gameClient.getId_player2().length() > 0;
    }

    public LobbyState(final GdxSpill game) {
        this.game = game;

        gamecam = new OrthographicCamera();
        gamePort = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), gamecam);
        stage = new Stage(gamePort, game.batch);

        bgTexture = new Texture("backgroundblur.jpg");
        player1 = new Texture("catRed.png");
        player2 = new Texture("catGreen.png");

        this.gameClient = game.gameClient;

        background = new TextureRegion(bgTexture, 0, 0, 4088, 2044);
        title = new Texture("Lobby.png");
        font = new BitmapFont();
    }

    @Override
    public void show() {
        // play button
        playBtn = new Image(new Texture(Gdx.files.internal("play.png"))); //Set the button up
        playBtn.setSize(playBtn.getWidth() / 3, playBtn.getHeight() / 2);
        playBtn.setPosition(Gdx.graphics.getWidth() - playBtn.getWidth() - 10, playBtn.getHeight() / 8);
        playBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("Play", "button clicked");
                playBtn = new Image(new Texture(Gdx.files.internal("play.png")));
                gameClient.playerIsReady();
                playerIsReady = true;
                dispose();
            }
        });

        // back  button
        backBtn = new Image(new Texture(Gdx.files.internal("back.png"))); //Set the button up
        backBtn.setSize(backBtn.getWidth() / 3, backBtn.getHeight() / 2);
        backBtn.setPosition(10, backBtn.getHeight() / 8);
        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("Back", "button clicked");
                game.gsm.setScreen(GameStateManager.STATE.MAIN_MENU);
                dispose();
            }
        });


        stage.addActor(playBtn); //Add the button to the stage to perform rendering and take input.
        stage.addActor(backBtn); //Add the button to the stage to perform rendering and take input.
        Gdx.input.setInputProcessor(stage); //Start taking input from the ui
    }

    public void update(float dt) {
        gameClient.isPlayerReady();
    }

    @Override
    public void render(float dt) {
        update(dt);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.batch.begin();
        game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.draw(title, Gdx.graphics.getWidth() / 2f - (float) title.getWidth() / 2f, Gdx.graphics.getHeight() - title.getHeight());
        game.batch.draw(player1, 50, 100, 200, 200);
        if (isTwoPlayers()) {
            game.batch.draw(player2,Gdx.graphics.getWidth() - player2.getWidth() * 2, 100, 200, 200);
        } else {
            font.draw(game.batch, "Waiting for player to connect...", 400, 150);
        }
        if(playerIsReady){
            game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            font.draw(game.batch, "Waiting for other player to get ready...", 50, Gdx.graphics.getHeight() / 2);
        }
        game.batch.end();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
