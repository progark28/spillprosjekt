package no.gruppe28.progark.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import no.gruppe28.progark.GdxSpill;
import no.gruppe28.progark.manager.GameStateManager;

public class MenuState implements Screen {

    private Texture title;
    private TextureRegion background;
    private Image playBtn, multiBtn, settingsBtn;

    private OrthographicCamera gamecam;
    private Viewport gamePort;

    private GdxSpill game;
    private Stage stage;


    public MenuState(final GdxSpill game) {
        this.game = game;

        gamecam = new OrthographicCamera();
        gamePort = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), gamecam);
        stage = new Stage(gamePort, game.batch);

        title = new Texture("title.png");
        background = new TextureRegion(new Texture("background.jpg"), 0, 0, 8000, 4000);

    }

    @Override
    public void show() {
        playBtn = new Image(new Texture(Gdx.files.internal("singleplayer.png"))); //Set the button up
        playBtn.setSize(playBtn.getWidth() / 2, playBtn.getHeight() / 2);
        playBtn.setPosition(Gdx.graphics.getWidth() / 2 - playBtn.getWidth() / 2, Gdx.graphics.getHeight() - playBtn.getHeight() * 4);
        playBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("Singleplayer", "button clicked");
                //game.gsm.setScreen(GameStateManager.STATE.PLAY, 1);

                //== tester init on-demand ==
                game.gsm.initAndSetPlay(1);
                dispose();
            }
        });

        multiBtn = new Image(new Texture(Gdx.files.internal("multiplayer.png"))); //Set the button up
        multiBtn.setSize(multiBtn.getWidth() / 2, multiBtn.getHeight() / 2);
        multiBtn.setPosition(Gdx.graphics.getWidth() / 2 - multiBtn.getWidth() / 2, Gdx.graphics.getHeight() - multiBtn.getHeight() * 5 - 10);
        multiBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("Mulitplayer", "button clicked");
                //game.gsm.setScreen(GameStateManager.STATE.LOBBY);

                //== tester init on-demand ===
                game.gsm.initAndSetLobby();
                dispose();
            }
        });

        settingsBtn = new Image(new Texture(Gdx.files.internal("settingsButton.png"))); //Set the button up
        settingsBtn.setSize(settingsBtn.getWidth() / 12, settingsBtn.getHeight() / 12);
        settingsBtn.setPosition(Gdx.graphics.getWidth() - settingsBtn.getWidth() - 10, Gdx.graphics.getHeight() - settingsBtn.getHeight() - 10);
        settingsBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("Settings", "button clicked");
                game.gsm.setScreen(GameStateManager.STATE.SETTINGS);
                dispose();
            }
        });


        stage.addActor(playBtn); //Add the button to the stage to perform rendering and take input.
        stage.addActor(multiBtn); //Add the button to the stage to perform rendering and take input.
        stage.addActor(settingsBtn);
        Gdx.input.setInputProcessor(stage); //Start taking input from the ui
    }


    @Override
    public void render(float dt) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.batch.begin();
        game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.draw(title, Gdx.graphics.getWidth() / 2 - title.getWidth() / 2, Gdx.graphics.getHeight() - title.getHeight());
        game.batch.end();

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        //game.dispose();
        //texture.dispose();
        //title.dispose();
    }
}
