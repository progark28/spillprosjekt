package no.gruppe28.progark.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;

import no.gruppe28.progark.GdxSpill;
import no.gruppe28.progark.components.Box;
import no.gruppe28.progark.components.Wooden_box;
import no.gruppe28.progark.manager.GameStateManager;
import no.gruppe28.progark.utils.BodyBuilder;
import no.gruppe28.progark.utils.Hud;

public class GameState implements Screen {

    private final String RED_BOX_PATH = "boxRed.png";
    private final String GREEN_BOX_PATH = "boxGreen.png";

    public static GdxSpill game;

    private boolean createNewBodyBoo = false;
    public int winnerPlayer = 0;    // brukes denne? i så fall hvor?

    private ArrayList<Box> boxes;
    private Box box1, box2, current_box;
    private Body ground;

    private Texture ground_tex, background_tex;
    private Image pauseBtn, playerImage;
    private Stage stage, stagePause;
    private Hud hud;

    private boolean isMultiplayer;
    private boolean boxColorToggle;
    private String thisPlayer;
    private String startPlayer;
    private String playerImagePath;

    private Sound thump;
    private Sound crashing;
    private Sound hitting;
    private Sound falling;
    private Sound thud;

    private long fallingID;


    public static OrthographicCamera gamecam;
    public static Viewport gamePort;

    public static World world;
    private Box2DDebugRenderer b2dr;



    public GameState(int players, final GdxSpill game) {
        this.game = game;

        if (game.gameClient != null && game.gameClient.getCurrentPlayer().length() > -1) {
            isMultiplayer = true;
            thisPlayer = game.gameClient.getId_player1();
            startPlayer = game.gameClient.getStartPlayer();
            playerImagePath = thisPlayer.equals(startPlayer) ? "catRed.png" : "catGreenFlipped.png";
        }


        gamecam = new OrthographicCamera();
        gamePort = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), gamecam);

        hitting = Gdx.audio.newSound(Gdx.files.internal("sound/HitWood.wav"));
        thump = Gdx.audio.newSound(Gdx.files.internal("sound/thump.wav"));
        crashing = Gdx.audio.newSound(Gdx.files.internal("sound/WoodDestruction.wav"));
        falling = Gdx.audio.newSound(Gdx.files.internal("sound/falling.wav"));
        fallingID = 0;
        thud = Gdx.audio.newSound(Gdx.files.internal("sound/thud.wav"));


        stage = new Stage(gamePort, game.batch);
        stagePause = new Stage();
        pauseBtn = new Image(new Texture(Gdx.files.internal("pause.png"))); //Set the button up
        playerImage = isMultiplayer ? new Image(new Texture(Gdx.files.internal(playerImagePath))) : new Image(new Texture(Gdx.files.internal("catRed.png")));
        hud = new Hud(players, gamePort);

        world = new World(new Vector2(0, -40), true);

        boxes = new ArrayList<>();

        box1 = isMultiplayer ? new Wooden_box(world, gamePort, gamecam.position.y - gamecam.viewportHeight / 2, RED_BOX_PATH) : new Wooden_box(world, gamePort, gamecam.position.y - gamecam.viewportHeight / 2);

        boxes.add(box1);
        current_box = box1;
        current_box.body.setGravityScale(0);
        current_box.body.setLinearVelocity(100, 0);

        b2dr = new Box2DDebugRenderer();

        background_tex = new Texture("sky.jpg");
        background_tex.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        ground_tex = new Texture("ground.png");
        ground = createWalls();



        listeners();

    }

    public String nextplayer() {
        return game.gameClient.getCurrentPlayer().equals(game.gameClient.getId_player1()) ? game.gameClient.getId_player2() : game.gameClient.getId_player1();
    }

    @Override
    public void show() {
        Gdx.app.postRunnable(new Runnable() {
            public void run() {
                playerImage.setSize(50, 50);
                playerImage.setPosition( 20, gamecam.viewportHeight - playerImage.getHeight()-20);
                pauseBtn.setSize(40, 40);
                pauseBtn.setPosition(gamecam.viewportWidth - 60, gamecam.viewportHeight - 60);
                pauseBtn.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Gdx.app.log("Pause", "button clicked");
                        System.out.println("x: " + x + " - y: " + y);
                        game.gsm.setScreen(GameStateManager.STATE.PAUSE);
                        dispose();
                    }
                });

                stagePause.addActor(pauseBtn); //Add the button to the stage to perform rendering and take input.
                stagePause.addActor(playerImage);
                Gdx.input.setInputProcessor(stagePause); //Start taking input from the ui
            }
        });
    }

    public boolean isMyTurn(){
        return game.gameClient.getId_player1().equals(game.gameClient.getCurrentPlayer());
    }

    public void handleInput() {

        // TODO: funker "Enter" for touch-devices? evt bytte til justTouched elns
        if (Gdx.input.isTouched()) {
            if (game.gameClient != null && isMyTurn()) {
                if(fallingID == 0 && game.isSound()){
                    fallingID = falling.play();
                    falling.setVolume(fallingID, 0.03f);
                }
                float x = current_box.body.getPosition().x;
                if (game.gameClient != null) {
                    game.gameClient.emitBox_posX(x);
                    game.gameClient.changePlayer(nextplayer());
                }
                current_box.body.setGravityScale(1);
                current_box.body.applyForceToCenter(world.getGravity(), true);
                current_box.body.setLinearVelocity(0, 0);

            } else if(game.gameClient == null){
                if(fallingID == 0 && game.isSound()){
                    fallingID = falling.play();
                    falling.setVolume(fallingID, 0.03f);
                }
                current_box.body.setGravityScale(1);
                current_box.body.applyForceToCenter(world.getGravity(), true);
                current_box.body.setLinearVelocity(0, 0);
            }
        }

    }

    public void handleServerInput(float x) {

        // TODO: funker "Enter" for touch-devices? evt bytte til justTouched elns
        if (x > 0) {
            current_box.body.setTransform(x, current_box.body.getPosition().y, current_box.body.getAngle());
            current_box.body.setGravityScale(1);
            current_box.body.applyForceToCenter(world.getGravity(), true);
            current_box.body.setLinearVelocity(0, 0);
        }
    }

    public void update(float delta) {
        handleInput();
        world.step(1.0f / 60.0f, 6, 2);

        if (current_box.body.getPosition().x >= Gdx.graphics.getWidth() - current_box.getWidth() / 2 || current_box.body.getPosition().x <= current_box.getWidth() / 2) {
            current_box.body.setLinearVelocity(current_box.body.getLinearVelocity().x * -1, 0);
        }
        createNewBox();
        gamecam.update();
        hud.update();

    }

    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        b2dr.render(world, gamecam.combined);
        game.batch.setProjectionMatrix(gamecam.combined);

        game.batch.begin();
        game.batch.draw(background_tex, gamecam.position.x - (gamecam.viewportWidth / 2), gamecam.position.y - (gamecam.viewportHeight / 2), gamecam.viewportWidth, gamecam.viewportHeight);
        game.batch.draw(ground_tex, ground.getPosition().x, ground.getPosition().y, gamePort.getScreenWidth(), 200);
        //game.batch.draw(playerImage, 10, gamecam.viewportHeight - playerImage.getHeight()/2, 50, 50 );
        // draw all boxes
        for (int i = 0; i < boxes.size(); i++) {
            boxes.get(i).update();
        }
        game.batch.end();

        stagePause.act(Gdx.graphics.getDeltaTime());
        stagePause.draw();
        stage.draw();

        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        thump.dispose();
        hitting.dispose();
        crashing.dispose();
        falling.dispose();
        thud.dispose();
    }

    private Body createWalls() {
        Vector2[] verts = new Vector2[5];
        verts[0] = new Vector2(-gamePort.getWorldWidth(), 0);
        verts[1] = new Vector2(gamePort.getWorldWidth(), 0);
        verts[2] = new Vector2(gamePort.getWorldWidth(), 20);
        verts[3] = new Vector2(-gamePort.getWorldWidth(), 20);
        verts[4] = new Vector2(-gamePort.getWorldWidth(), 0);

        return BodyBuilder.createChainShape(world, verts);
    }

    private void listeners() {

        world.setContactListener(new ContactListener() {

            @Override
            public void beginContact(Contact contact) {
                // her er fixA er kontakt med bakken
                Fixture fixA = contact.getFixtureA();
                Fixture fixB = contact.getFixtureB();
                Gdx.app.log("begin contact",
                        "between " + fixA.getBody().getType().name() + " and " + fixB.getBody().getType().name());

                if (fixA.getBody() == ground) {
                    if (fixB.getBody() == box1.body) {
                        if(game.isSound()) {
                            falling.stop();
                            fallingID = 0;
                            long hittingID = hitting.play();
                            hitting.setVolume(hittingID, 0.5f);
                        }
                        // slipp ny boks
                        Gdx.app.log("ground + box1", "ehi");
                        createNewBodyBoo = true;
                    } else {
                        if(game.isSound()) {
                            long crashingID = crashing.play();
                            falling.stop();
                            crashing.setVolume(crashingID, 0.06f);
                        }
                        Gdx.app.log("ok", "won is set");
                        //TODO: må fikse sånn at spillet restartes
                        //TODO: må kunne sende med verdien fra hud.getplayer() for å si hvem som vant
                        game.gsm.setGameFinished(true);
                        game.gsm.setScreen(GameStateManager.STATE.WON);
                        // change state!
                        return;
                    }
                    //Er det alltid fixB som blir currentbox?
                } else if (fixB.getBody() == current_box.body) {
                    if(game.isSound()) {
                        long hittingID = hitting.play();
                        falling.stop();
                        fallingID = 0;
                        hitting.setVolume(hittingID, 0.5f);
                    }
                    // slipp ny boks
                    Gdx.app.log("pos", "x: " + gamecam.position.x + "y: " + gamecam.position.y);
                    createNewBodyBoo = true;
                    Gdx.app.log("FixB", "box");
                }
                System.out.print("Hit kom jeg");
            }

            @Override
            public void endContact(Contact contact) {

            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });
    }

    private void createNewBox() {

        if (createNewBodyBoo) {
            //TODO: dersom boksen kommer til å falle av garantert, vil det fortsatt opprettes en
            // ny boks (altså det blir neste spiller) og spilleren som feilet vil få poeng
            hud.setScore(hud.getPlayer());
            hud.changePlayer();
            winnerPlayer = hud.getPlayer();
            gamecam.translate(0, current_box.getHeight() / 4);
            // burde nesten løse dette på en annen måte. blir fortsatt ikke veldig visuelt eller en
            // gradvis forflytning, men det er en midlertidig fiks for at man kan spille over lengre tid.
            if (boxes.size() % 5 == 0) {
                for (int i = 0; i < 16; i++) {
                    gamecam.translate(0, current_box.getHeight() * 3 / 16);
                }
            }
            ;

            if(isMultiplayer){
                box2 = boxColorToggle ? new Wooden_box(world, gamePort, gamecam.position.y - gamecam.viewportHeight / 2, RED_BOX_PATH) : new Wooden_box(world, gamePort, gamecam.position.y - gamecam.viewportHeight / 2, GREEN_BOX_PATH);
                boxColorToggle = !boxColorToggle;
            } else{
                box2 = new Wooden_box(world, gamePort, gamecam.position.y - gamecam.viewportHeight / 2);
            }
            createNewBodyBoo = false;
            current_box = box2;
            current_box.body.setGravityScale(0);
            current_box.body.setLinearVelocity(100, 0);
            boxes.add(box2);
        }

    }

//TODO: kan denne fjernes?

//    public int getWinner(){
//        return winnerPlayer;
//    }

}
