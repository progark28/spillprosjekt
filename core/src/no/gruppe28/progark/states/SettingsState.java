package no.gruppe28.progark.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import no.gruppe28.progark.GdxSpill;
import no.gruppe28.progark.manager.GameStateManager;

public class SettingsState implements Screen {
    private GdxSpill game;
    private Stage stage;

    private OrthographicCamera gamecam;
    private Viewport gamePort;

    private Texture texture, title;
    private TextureRegion background;
    private Image backBtn;
    private Image playBtn;
    private Image musicButton;
    private Image playing;
    private Image mute;


    public SettingsState(GdxSpill game){
        this.game = game;

        gamecam = new OrthographicCamera();
        gamePort = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), gamecam);
        stage = new Stage(gamePort, game.batch);

        texture = new Texture("backgroundblur.jpg");
        background = new TextureRegion(texture, 0, 0 , 4088, 2044);
        title = new Texture("Settings.png");
        playing = new Image(new Texture(Gdx.files.internal("SoundOn.PNG" )));
        mute = new Image(new Texture(Gdx.files.internal("SoundOff.PNG")));
    }

    @Override
    public void show() {
        musicButton = game.isSound() ? playing : mute; //Set the button up
        musicButton.setSize(musicButton.getWidth()/3, musicButton.getHeight()/2);
        musicButton.setPosition(Gdx.graphics.getWidth() - musicButton.getWidth() - 10,  musicButton.getHeight()/8);
        musicButton.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(game.isSound()) {
                    game.setSound(false);
                                    }
                else {
                    game.setSound(true);
                    musicButton = new Image(new Texture(Gdx.files.internal(game.isSound() ? "SoundOn.PNG" : "SoundOff.PNG")));
                }
                dispose();
            }
        });

        backBtn = new Image(new Texture(Gdx.files.internal("back.png"))); //Set the button up
        backBtn.setSize(backBtn.getWidth()/3, backBtn.getHeight()/2);
        backBtn.setPosition(10, backBtn.getHeight()/8);
        backBtn.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("Back", "button clicked");
                game.gsm.setScreen(GameStateManager.STATE.MAIN_MENU);
                dispose();
            }
        });


        stage.addActor(musicButton); //Add the button to the stage to perform rendering and take input.
        stage.addActor(backBtn); //Add the button to the stage to perform rendering and take input.
        Gdx.input.setInputProcessor(stage); //Start taking input from the ui
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.batch.begin();
        game.batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.batch.draw(title, Gdx.graphics.getWidth() / 2 - title.getWidth() / 2, Gdx.graphics.getHeight() - title.getHeight());
        game.batch.end();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
