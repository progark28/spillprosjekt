package no.gruppe28.progark.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import no.gruppe28.progark.GdxSpill;
import no.gruppe28.progark.manager.GameStateManager;

public class PauseState implements Screen {
    private Texture texture, pauseTxt;

    private OrthographicCamera gamecam;
    private Viewport gamePort;

    private GdxSpill game;
    private Stage stage;

    private Image quit_button, settingsBtn, play_button;

    public PauseState(final GdxSpill game){
        this.game = game;

        gamecam = new OrthographicCamera();
        gamePort = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), gamecam);
        stage = new Stage(gamePort, game.batch);

        texture = new Texture("sky.jpg");
        texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

        pauseTxt = new Texture("PAUSEtxt.png");
    }

    @Override
    public void show() {
        play_button = new Image(new Texture(Gdx.files.internal("resume.png"))); //Set the button up
        play_button.setSize(play_button.getWidth()/3, play_button.getHeight()/2);
        play_button.setPosition(Gdx.graphics.getWidth()/2 - play_button.getWidth()/2, Gdx.graphics.getHeight() - play_button.getHeight()*4);
        play_button.addListener(new ClickListener() {
                                    @Override
                                    public void clicked(InputEvent event, float x, float y) {
                                        Gdx.app.log("Resume", "button clicked");
                                        game.gsm.setScreen(GameStateManager.STATE.PLAY);
                                        dispose();
                                    }
                                });

        quit_button = new Image(new Texture(Gdx.files.internal("quit.png"))); //Set the button up
        quit_button.setSize(quit_button.getWidth()/3, quit_button.getHeight()/2);
        quit_button.setPosition(Gdx.graphics.getWidth()/2 - quit_button.getWidth()/2, Gdx.graphics.getHeight() - quit_button.getHeight()*5 - 10);
        quit_button.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("Quit", "button clicked");
                // TODO: kan nå kanskje bli problemer hvis man avslutter et multiplayerspill?
                game.gsm.initPlay();
                game.gsm.setScreen(GameStateManager.STATE.MAIN_MENU);
                dispose();
            }
        });

        // TODO: fikse hvordan setting skal gå mellom menustate og pausestate
        settingsBtn = new Image(new Texture(Gdx.files.internal("settingsButton.png"))); //Set the button up
        settingsBtn.setSize(settingsBtn.getWidth()/12, settingsBtn.getHeight()/12);
        settingsBtn.setPosition(Gdx.graphics.getWidth() - settingsBtn.getWidth() - 10 , Gdx.graphics.getHeight() - settingsBtn.getHeight() - 10);
        settingsBtn.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("Settings", "button clicked");
                game.gsm.setScreen(GameStateManager.STATE.SETTINGS);
                dispose();
            }
        });


        stage.addActor(quit_button); //Add the button to the stage to perform rendering and take input.
        stage.addActor(play_button); //Add the button to the stage to perform rendering and take input.
        stage.addActor(settingsBtn);
        Gdx.input.setInputProcessor(stage); //Start taking input from the ui


    }


    @Override
    public void render(float dt) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.batch.begin();
        game.batch.draw(texture, gamecam.position.x - (gamecam.viewportWidth / 2), gamecam.position.y- (gamecam.viewportHeight / 2), gamecam.viewportWidth, gamecam.viewportHeight);
        game.batch.draw(pauseTxt, Gdx.graphics.getWidth()/2 - pauseTxt.getWidth()/2, Gdx.graphics.getHeight() - pauseTxt.getHeight());
        game.batch.end();

        stage.act(Gdx.graphics.getDeltaTime()); //Perform ui logic
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
