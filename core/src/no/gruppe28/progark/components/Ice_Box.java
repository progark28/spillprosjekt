package no.gruppe28.progark.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.Viewport;


public class Ice_Box extends Box {
    public Ice_Box(World world, Viewport gamePort, float y) {
        super(gamePort, y);
        box = new Sprite(new Texture("iceblock.png"));
        width = 40;
        height = 40;
        createBox(world, width, height, 3, 0);
    }
    public Ice_Box(World world, Viewport gamePort, float y, String filename) {
        super(gamePort, y);
        box = new Sprite(new Texture(filename));
        width = 40;
        height = 40;
        createBox(world, width, height, 3, 0);
    }
}


