package no.gruppe28.progark.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.Viewport;

public abstract class Box {

    protected Sprite box;
    public Body body;
    protected float width;
    protected float height;
    protected Vector2 pos;

    public Box(Viewport gameport, float y){
        this.pos = new Vector2(gameport.getWorldWidth() / 2, gameport.getWorldHeight() + y);

    }

protected void createBox(World world,  float width, float height, float density, float friction){
    BodyDef bDef = new BodyDef();
    bDef.type =BodyDef.BodyType.DynamicBody;
    bDef.fixedRotation = false;
    bDef.position.set(pos.x + width/2, pos.y - height);
    Body body = world.createBody(bDef);

    PolygonShape shape = new PolygonShape();
    shape.setAsBox(width/2, height/2);

    FixtureDef fDef = new FixtureDef();
    fDef.shape = shape;
    fDef.density = density;
    fDef.friction = friction;
    body.createFixture(fDef);
    shape.dispose();
    this.body = body;
}

    public void update() {
        box.setRotation((float)(body.getAngle() * 180.f / Math.PI));
        box.setPosition(body.getPosition().x - 20, body.getPosition().y - 20);
        no.gruppe28.progark.states.GameState.game.batch.draw(box, box.getX(), box.getY(), width/2, height/2, width, height, 1, 1,
                (float) (body.getAngle() * 180.f / Math.PI));

    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
